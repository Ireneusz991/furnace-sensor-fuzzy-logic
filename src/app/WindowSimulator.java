package app;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class WindowSimulator {

    private static JFrame mainFrame;

    private static JLabel statusLabel;

    private static JTextPane txtTempIn;
    private static JTextPane txtTempOut;
    private static JTextPane txtSpeed;
    private static JTextPane txtCzas;

    private static JSpinner speedSpinner;
    private static JSpinner seasonSpinner;

    private static JButton btnTempOut;
    private static JButton btnTempIn;
    private static JButton btnSpeed;


    private static JPanel panelChart;
    private static JPanel panelInfo;

    private static JTabbedPane chartOut;

    private static boolean isEnable = false;
    private static boolean isStart = false;

    private static Color panelColor = new Color(0, 255, 255);
    private static Color chartColor = new Color(150, 50, 100);

    private static XYSeries seriesPowerCooler;

    private static JTextField txtTemperOut;
    private static JTextField txtTemperIn;
    private static JTextField txtTemperCooler;
    private static JTextField txtTempSet;
    private static JTextField txtEditTempIn;

    private static int predkoscSymulatoraMs = 1000;
    private static int predkoscSymulatoraNs = 0;

    private static float tempIn, tempOut;
    private static float temperatureInside = 10f;
    private static float deltaTemperature;
    private static float moduleClima;

    private static String season;
    private static String choseSpeed;

    private static Zegar zegar = new Zegar();

    static int[] spring = new int[] {-1, -2, -1, -1, 0, 1, 2, 3, 5, 7, 8, 8, 9, 9, 10, 9, 8, 7, 6, 6, 4, 3, 2, 0};
    static int[] hotSpring = new int[] {6,5,5,6,7,8,9,10,10,11,12,13,14,15,16,16,15,14,12,11,10,8,7,6};
    static int[] summer = new int[] {13,12,12,13,14,15,16,17,19,20,22,24,25,25,25,24,24,22,20,19,17,15,14,13};
    static int[] hotSummer = new int[] {20,19,19,20,21,22,22,23,25,26,27,28,30,32,32,30,28,26,25,24,23,22,21,20};
    static int[] autumn = new int[] {1,1,1,3,4,5,7,8,9,10,10,11,12,12,11,10,9,8,7,6,5,4,3,2};
    static int[] hotAutumn = new int[] {8,7,7,8,9,10,11,12,14,15,17,19,20,21,20,19,18,17,15,14,12,11,9,8};
    static int[] winter = new int[] {-15, -15, -16, -16, -17, -17, -16, -15, -13, -12, -11, -10, -10, -9, -9, -9, -10, -11,-11, -12, -13, -13, -14, -14};
    static int[] hotWinter = new int[] {-6,-6,-7,-7,-6,-5,-5,-4,-3,-2,-1,-1,0,0,-1,-1,-2,-2,-3,-4,-5,-5,-6,-6};


    public static void createPanel() {
        mainFrame = new JFrame("Chłodziarka");
        mainFrame.getContentPane().setBackground(SystemColor.control);
        mainFrame.setSize(1200, 600);
        mainFrame.setResizable(false);
        mainFrame.setDefaultCloseOperation(mainFrame.EXIT_ON_CLOSE);
        mainFrame.setLayout(new FlowLayout());

        JButton add = new JButton("Przesuń o godzinę");
        add.setBounds(160,400,130,32);
        add.setPreferredSize(new Dimension(5,5));

        add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                zegar.DodajGodznie();
            }
        });
        mainFrame.add(add);


        JButton btn = new JButton("Start");
        btn.setBounds(160, 440, 130, 32);
        btn.setPreferredSize(new Dimension(5, 5));

        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isStart == false) {
                    statusLabel.setText("Uruchomiono symulacje");
                    btn.setText("Zatrzymaj");
                    isStart = true;
                    isEnable = true;
                    Symulacja();

                } else {
                    if (isEnable == true) {
                        statusLabel.setText("Zatrzymano symulator");
                        isEnable = false;
                        btn.setText("Wznów");
                    } else {
                        statusLabel.setText("Wznowiono działanie symulatora");
                        isEnable = true;
                        btn.setText("Zatrzymaj");
                    }
                }
            }
        });

        mainFrame.getContentPane().setLayout(null);
        mainFrame.getContentPane().add(btn);

        statusLabel = new JLabel("Apka", SwingConstants.LEFT);
        statusLabel.setBounds(68, 531, 700, 20);
        mainFrame.getContentPane().add(statusLabel);

        JPanel panel = new JPanel();
        panel.setBackground(panelColor);
        panel.setBorder(new CompoundBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)), "", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(64, 64, 64)), null));
        panel.setBounds(75, 30, 300, 350);
        mainFrame.getContentPane().add(panel);
        panel.setLayout(null);

        JTextPane txtParam = new JTextPane();
        txtParam.setFont(new Font("Tahoma", Font.BOLD, 19));
        txtParam.setEditable(false);
        txtParam.setBounds(45, 11, 210, 36);
        txtParam.setBackground(panelColor);
        txtParam.setForeground(SystemColor.windowText);
        panel.add(txtParam);
        txtParam.setText("Parametry Programu");

        txtTempIn = new JTextPane();
        txtTempIn.setBackground(panelColor);
        txtTempIn.setText("Temperatura w środku");
        txtTempIn.setBounds(80, 45, 140, 20);
        panel.add(txtTempIn);

        txtEditTempIn = new JTextField();
        txtEditTempIn.setBounds(80, 65, 140, 25);
        txtEditTempIn.setColumns(10);
        panel.add(txtEditTempIn);

        btnTempIn = new JButton("Zmień Temperature");
        btnTempIn.setFont(new Font("Tahoma", Font.PLAIN, 11));
        btnTempIn.setBounds(80, 95, 140, 25);
        panel.add(btnTempIn);
        btnTempIn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    tempIn = Float.parseFloat(txtEditTempIn.getText());

                    if (tempIn <= 40 && tempIn >= -40) {
                        statusLabel.setText("Ustawiono temperature oczekiwaną");
                        txtTemperIn.setText(String.valueOf(tempIn));
                    } else {
                        tempIn = 0;
                        statusLabel.setText("Wprawadz temperature z zakresu -40> x <40");
                    }
                } catch (NumberFormatException nfe) {
                    statusLabel.setText("Wprowadzono niepoprawną wartość !");
                }
            }
        });
//////////////////////////////////////////////////////////////////

        txtTempOut = new JTextPane();
        txtTempOut.setBackground(panelColor);
        txtTempOut.setText("Pora roku");
        txtTempOut.setBounds(120, 130, 170, 20);
        panel.add(txtTempOut);

        seasonSpinner = new JSpinner();
        seasonSpinner.setBounds(80, 155, 140, 25);
        seasonSpinner.setModel(new SpinnerListModel(new String[]{"Ciepla wiosna","Chlodna wiosna","Cieple lato",
                "Chlodne lato","Ciepla jesien", "Chlodna jesien","Ciepla zima","Chlodna zima"}));
        panel.add(seasonSpinner);

        btnTempOut = new JButton("Zmień porę roku");
        btnTempOut.setFont(new Font("Tahoma", Font.PLAIN, 11));
        btnTempOut.setBounds(80, 185, 140, 25);
        panel.add(btnTempOut);
        btnTempOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                season = String.valueOf(seasonSpinner.getValue());
                statusLabel.setText("Zmieniono porę roku na: " + season);
            }
        });

//////////////////////////////////////////////////////////////////

        txtSpeed = new JTextPane();
        txtSpeed.setBackground(panelColor);
        txtSpeed.setText("Prędkość symulacji");
        txtSpeed.setBounds(80, 220, 170, 20);
        panel.add(txtSpeed);

        JSpinner speedSpinner = new JSpinner();
        speedSpinner.setModel(new SpinnerListModel(new String[]{"x1", "x2", "x5", "x10", "x100", "x1000"}));
        speedSpinner.setBounds(80, 245, 140, 25);
        panel.add(speedSpinner);

        btnSpeed = new JButton("Zmień Predkosc");
        btnSpeed.setFont(new Font("Tahoma", Font.PLAIN, 11));
        btnSpeed.setBounds(80, 275, 140, 25);
        panel.add(btnSpeed);
        btnSpeed.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                choseSpeed = String.valueOf(speedSpinner.getValue());
                statusLabel.setText("Zmieniono prędkosc symulacji na: " + choseSpeed);
                setSpeedSimulator(choseSpeed);
            }
        });

        txtCzas = new JTextPane();
        txtCzas.setEditable(false);
        txtCzas.setBounds(50, 310, 200, 25);
        panel.add(txtCzas);



//////////////////////////////////////////////////////////////////

        panelChart = new JPanel();
        panelChart.setBackground(chartColor);
        panelChart.setBounds(425, 30, 700, 350);
        mainFrame.getContentPane().add(panelChart);
        panelChart.setLayout(null);

        chartOut = new JTabbedPane(JTabbedPane.TOP);
        chartOut.setBackground(SystemColor.control);
        chartOut.setBounds(0, 0, 700, 350);
        panelChart.add(chartOut);

        new JTabbedPane(JTabbedPane.TOP);
        seriesPowerCooler = new XYSeries("Moc chłodni");
        XYSeriesCollection dataset = new XYSeriesCollection(seriesPowerCooler);
        JFreeChart chart = ChartFactory.createXYLineChart("Moc Chłodnicza", "Czas (sekunda)", "Moc", dataset);
        chartOut.add(new ChartPanel(chart), "Moc");

/////////////////////////////////////////////////////////

        panelInfo = new JPanel();
        panelInfo.setBackground(panelColor);
        panelInfo.setBounds(425, 400, 700, 100);
        mainFrame.getContentPane().add(panelInfo);
        panelInfo.setLayout(null);

        JLabel lblTempOut = new JLabel("Temperatura zewnętrzna");
        lblTempOut.setHorizontalAlignment(SwingConstants.CENTER);
        lblTempOut.setBackground(SystemColor.control);
        lblTempOut.setBounds(30, 25, 150, 15);
        panelInfo.add(lblTempOut);

        txtTemperOut = new JTextField();
        txtTemperOut.setHorizontalAlignment(SwingConstants.CENTER);
        txtTemperOut.setEditable(false);
        txtTemperOut.setBounds(40, 45, 130, 20);
        panelInfo.add(txtTemperOut);
        txtTemperOut.setColumns(10);

        JLabel lblTempIn = new JLabel("Temperatura wewnetrzna");
        lblTempIn.setHorizontalAlignment(SwingConstants.CENTER);
        lblTempIn.setBounds(220, 25, 150, 14);
        panelInfo.add(lblTempIn);

        txtTemperIn = new JTextField();
        txtTemperIn.setHorizontalAlignment(SwingConstants.CENTER);
        txtTemperIn.setEditable(false);
        txtTemperIn.setBounds(230, 45, 130, 20);
        panelInfo.add(txtTemperIn);
        txtTemperIn.setColumns(10);

        JLabel coolerPower = new JLabel("Moc chłodni");
        coolerPower.setHorizontalAlignment(SwingConstants.CENTER);
        coolerPower.setBounds(400, 25, 120, 14);
        panelInfo.add(coolerPower);

        txtTemperCooler = new JTextField();
        txtTemperCooler.setHorizontalAlignment(SwingConstants.CENTER);
        txtTemperCooler.setEditable(false);
        txtTemperCooler.setBounds(410, 45, 100, 20);
        panelInfo.add(txtTemperCooler);
        txtTemperCooler.setColumns(10);

        JLabel tempSet = new JLabel("Temperatura obecna");
        tempSet.setHorizontalAlignment(SwingConstants.CENTER);
        tempSet.setBounds(550, 25, 120, 14);
        panelInfo.add(tempSet);

        txtTempSet = new JTextField();
        txtTempSet.setHorizontalAlignment(SwingConstants.CENTER);
        txtTempSet.setEditable(false);
        txtTempSet.setBounds(560, 45, 100, 20);
        panelInfo.add(txtTempSet);
        txtTempSet.setColumns(10);
        mainFrame.setVisible(true);
    }

    private static void Symulacja() {
        temperatureInside = 0f;

        SwingWorker sw1 = new SwingWorker() {
            @Override
            protected Object doInBackground() throws Exception {

                int i = 0;
                for (;;) {
                    if (isEnable == true) {
                        float tempOutside;


                        tempOut = GetTemperature();
                        Thread.sleep(predkoscSymulatoraMs, predkoscSymulatoraNs);
                        zegar.ZmienZegar();
                        txtCzas.setText("Aktualna godzina: " + zegar.zwrocCzas());

                        tempOutside = GetTemperature();

                        if(temperatureInside>tempOutside){
                            temperatureInside = temperatureInside - ((temperatureInside-tempOutside)*0.00005f);
                        }else if(temperatureInside<tempOutside){
                            temperatureInside = temperatureInside - ((temperatureInside + tempOutside)*0.00005f);
                        }else {
                            temperatureInside = temperatureInside;
                        }


                        deltaTemperature =   tempIn - temperatureInside;
                        moduleClima = UIEngine.CalculatePower(deltaTemperature);
                        temperatureInside = temperatureInside + ((64f*deltaTemperature)/3600f);

                        txtTemperOut.setText(String.valueOf(tempOut));
                        txtTemperCooler.setText(String.valueOf(moduleClima));
                        txtTempSet.setText(String.valueOf(temperatureInside));

                        seriesPowerCooler.add(i,moduleClima);
                        System.out.println(temperatureInside);
                        System.out.println(moduleClima);
                        System.out.println(tempOut);


                    i++;
                    } else {
                        Thread.sleep(100);
                    }
                }
            }
        };
        sw1.execute();
    }

    private static float GetTemperature(){

        int temp = 0;

        if (season == "Chlodna zima"){
            temp = winter[zegar.zwrocGodzina()];
        } else if (season == "Ciepla zima"){
            temp = hotWinter[zegar.zwrocGodzina()];
        } else if (season == "Chlodna jesien"){
            temp = autumn[zegar.zwrocGodzina()];
        } else if (season == "Ciepla jesien"){
            temp = hotAutumn[zegar.zwrocGodzina()];
        } else if (season == "Chlodne lato"){
            temp = summer[zegar.zwrocGodzina()];
        } else if (season == "Cieple lato"){
            temp = hotSummer[zegar.zwrocGodzina()];
        } else if (season == "Chlodna wiosna"){
            temp = spring[zegar.zwrocGodzina()];
        } else if (season == "Ciepla wiosna"){
            temp = hotSpring[zegar.zwrocGodzina()];
        }
        return temp;
    }

    private static void setSpeedSimulator(String speeed){
        switch (speeed) {
            case "x1": {
                predkoscSymulatoraMs = 1000;
                break;
            }
            case "x2": {
                predkoscSymulatoraMs = 500;
                break;
            }
            case "x5": {
                predkoscSymulatoraMs = 200;
                break;
            }
            case "x10": {
                predkoscSymulatoraMs = 100;
                break;
            }
            case "x100": {
                predkoscSymulatoraMs = 10;
                break;
            }
            case "x1000": {
                predkoscSymulatoraMs = 1;
                break;
            }
        }
    }
}