package app;

import jxl.*;

import java.io.File;

public class UIEngine {

    /**
     * Pomocnicze reguły zawierające reguly temperatury/klimatyzacji,
     * zmienna tymczasowa
     */
    private static String[][] rules;
    /**
     * Reguły klimatyzacji sterujące jej wartością
     */
    private static String[] rulesFreezer;
    /**
     * Zmienna lingwistyczna Temperatura
     */
    private static Float[][] tabTempFuzzy;
    /**
     * Zmienna lingwistyczna Chłodni
     */
    private static Float[][] tabFreezerFuzzy;



    /**
     * Metoda odpowiedzialna za tworzenie zmiennej lingwistycznych Chłodziarki
     */


    public static void DeltaFreezerFuzzy() {
        tabFreezerFuzzy = new Float[10][1000];
        float a = -5f;
        float b = -4f;
        float c = -3f;
        float d = -2f;
        float e = -1f;
        float f = 0f;
        float g = 1f;
        float h = 2f;
        float i = 3f;
        float j = 4f;
        float k = 5f;

        float variable = -500f;
        for (int counter = 0; counter < tabFreezerFuzzy[0].length; counter++) {

            tabFreezerFuzzy[0][counter] = variable / 100f;

            //ekstremalnie niska
            if((tabFreezerFuzzy[0][counter] >= a) && ( tabFreezerFuzzy[0][counter] <= b)) {
                tabFreezerFuzzy[1][counter] = 1f;
            } else if ((tabFreezerFuzzy[0][counter] > b)&& (tabFreezerFuzzy[0][counter] < c)){
                tabFreezerFuzzy[1][counter] = (c - tabFreezerFuzzy[0][counter])/ (c-b);
            }else {
                tabFreezerFuzzy[1][counter]=0f;
            }

            //bardzo niska
            if((tabFreezerFuzzy[0][counter] >= b) && (tabFreezerFuzzy[0][counter] < c)) {
                tabFreezerFuzzy[2][counter] = (tabFreezerFuzzy[0][counter] - b) / (c-b);
            } else if((tabFreezerFuzzy[0][counter] >= c) && (tabFreezerFuzzy[0][counter] < d)) {
                    tabFreezerFuzzy[2][counter] = (d - tabFreezerFuzzy[0][counter]) / (d-c);
            } else {
                    tabFreezerFuzzy[2][counter] = 0f;
                }


            //niska
            if((tabFreezerFuzzy[0][counter] >= c) && (tabFreezerFuzzy[0][counter] < d)) {
                tabFreezerFuzzy[3][counter] = (tabFreezerFuzzy[0][counter] - c) / (d-c);
            } else if((tabFreezerFuzzy[0][counter] >= d) && (tabFreezerFuzzy[0][counter] < e)) {
                tabFreezerFuzzy[3][counter] = (e - tabFreezerFuzzy[0][counter]) / (e-d);
            } else {
                tabFreezerFuzzy[3][counter] = 0f;
            }


            //trochę niska
            if((tabFreezerFuzzy[0][counter] >= d) && (tabFreezerFuzzy[0][counter] < e)) {
                tabFreezerFuzzy[4][counter] = (tabFreezerFuzzy[0][counter] - d) / (e-d);
            } else if((tabFreezerFuzzy[0][counter] >= e) && (tabFreezerFuzzy[0][counter] < f)) {
                tabFreezerFuzzy[4][counter] = (f - tabFreezerFuzzy[0][counter]) / (f-e);
            } else {
                tabFreezerFuzzy[4][counter] = 0f;
            }

            //optymalna
            if((tabFreezerFuzzy[0][counter] >= e) && (tabFreezerFuzzy[0][counter] < f)) {
                tabFreezerFuzzy[5][counter] = (tabFreezerFuzzy[0][counter] - e) / (f-e);
            } else if((tabFreezerFuzzy[0][counter] >= f) && (tabFreezerFuzzy[0][counter] < g)) {
                    tabFreezerFuzzy[5][counter] = (g - tabFreezerFuzzy[0][counter]) / (g-f);
            } else {
                tabFreezerFuzzy[5][counter] = 0f;
            }

            //trochę wysoka
            if((tabFreezerFuzzy[0][counter] >= f) && (tabFreezerFuzzy[0][counter] < g)) {
                tabFreezerFuzzy[6][counter] = (tabFreezerFuzzy[0][counter] - f) / (g-f);
            } else if((tabFreezerFuzzy[0][counter] >= g) && (tabFreezerFuzzy[0][counter] < h)) {
                tabFreezerFuzzy[6][counter] = (h - tabFreezerFuzzy[0][counter]) / (h-g);
            } else {
                tabFreezerFuzzy[6][counter] = 0f;
            }

            //wysoka
            if((tabFreezerFuzzy[0][counter] >= g) && (tabFreezerFuzzy[0][counter] < h)) {
                tabFreezerFuzzy[7][counter] = (tabFreezerFuzzy[0][counter] - g) / (h-g);
            } else if((tabFreezerFuzzy[0][counter] >= h) && (tabFreezerFuzzy[0][counter] < i)) {
                tabFreezerFuzzy[7][counter] = (i - tabFreezerFuzzy[0][counter]) / (i-h);
            } else {
                tabFreezerFuzzy[7][counter] = 0f;
            }

            //bardzo wysoka
            if((tabFreezerFuzzy[0][counter] >= h) && (tabFreezerFuzzy[0][counter] < i)) {
                tabFreezerFuzzy[8][counter] = (tabFreezerFuzzy[0][counter] - h) / (i-h);
            } else if((tabFreezerFuzzy[0][counter] >= i) && (tabFreezerFuzzy[0][counter] < j)) {
                tabFreezerFuzzy[8][counter] = (j - tabFreezerFuzzy[0][counter]) / (j-i);
            } else {
                tabFreezerFuzzy[8][counter] = 0f;
            }

            //ekstremalnie wysoka
            if((tabFreezerFuzzy[0][counter] >= i) && (tabFreezerFuzzy[0][counter] < j)) {
                tabFreezerFuzzy[9][counter] = (tabFreezerFuzzy[0][counter] - i) / (j-i);
            } else if((tabFreezerFuzzy[0][counter] >= j) && (tabFreezerFuzzy[0][counter] < k)) {
                tabFreezerFuzzy[9][counter] = (k - tabFreezerFuzzy[0][counter]) / (k-j);
            } else {
                tabFreezerFuzzy[9][counter] = 0f;
            }

            variable = variable +1;
            System.out.println(tabFreezerFuzzy[0][counter] + " | " + tabFreezerFuzzy[1][counter] + " | " + tabFreezerFuzzy[2][counter] + " | " + tabFreezerFuzzy[3][counter] + " | " + tabFreezerFuzzy[4][counter] + " | " +tabFreezerFuzzy[5][counter] + " | " +tabFreezerFuzzy[6][counter] + " | " +tabFreezerFuzzy[7][counter] + " | " + tabFreezerFuzzy[8][counter] + " | " + tabFreezerFuzzy[9][counter] + " | ");
        }


    }




    /**
     * Metoda odpowiedzialna za tworzenie zmiennej lingwistycznych Temperatury
     */
    public static void DeltaTempFuzzy() {
        float tempIn = 5f;
        tabTempFuzzy = new Float[10][801];

        float a = -40f;
        float b = -27f;
        float c = -9f;
        float d = -3f;
        float e = 0f;
        float f = 3f;
        float g = 9f;
        float h = 27f;
        float i = 40f;




        int variable = -400;

        for (int counter = 0; counter < tabTempFuzzy[0].length; counter++) {


            tabTempFuzzy[0][counter] = variable / 10f;

            //ekstremalnie niska
            if ((tabTempFuzzy[0][counter] >= a) && (tabTempFuzzy[0][counter] <= b)) {
                tabTempFuzzy[1][counter] = (c - tabTempFuzzy[0][counter]) / (c - b);
            } else {
                tabTempFuzzy[1][counter] = 0f;
            }

            // bardzo niska
            if ((tabTempFuzzy[0][counter] >= a) && (tabTempFuzzy[0][counter] <= b)) {
                tabTempFuzzy[2][counter] = (tabTempFuzzy[0][counter] - a) / (b - a);
            } else if ((tabTempFuzzy[0][counter] >= b) && (tabTempFuzzy[0][counter] <= c)) {
                tabTempFuzzy[2][counter] = (c - tabTempFuzzy[0][counter]) / (c - b);
            } else {
                tabTempFuzzy[2][counter] = 0f;
            }

            // niska
            if ((tabTempFuzzy[0][counter] >= b) && (tabTempFuzzy[0][counter] <= c)) {
                tabTempFuzzy[3][counter] = (tabTempFuzzy[0][counter] - b) / (c - b);
            } else if ((tabTempFuzzy[0][counter] >= c) && (tabTempFuzzy[0][counter] <= d)) {
                tabTempFuzzy[3][counter] = (d - tabTempFuzzy[0][counter]) / (d - c);
            } else {
                tabTempFuzzy[3][counter] = 0f;
            }

            // troche niska
            if ((tabTempFuzzy[0][counter] >= c) && (tabTempFuzzy[0][counter] <= d)) {
                tabTempFuzzy[4][counter] = (tabTempFuzzy[0][counter] - c) / (d - c);
            } else if ((tabTempFuzzy[0][counter] >= d) && (tabTempFuzzy[0][counter] <= e)) {
                tabTempFuzzy[4][counter] = (e - tabTempFuzzy[0][counter]) / (e - d);
            } else {
                tabTempFuzzy[4][counter] = 0f;
            }

            // optymalna
            if ((tabTempFuzzy[0][counter] >= d) && (tabTempFuzzy[0][counter] <= e)) {
                tabTempFuzzy[5][counter] = (tabTempFuzzy[0][counter] - d) / (e - d);
            } else if ((tabTempFuzzy[0][counter] > e) && (tabTempFuzzy[0][counter] < f)) {
                tabTempFuzzy[5][counter] = (f - tabTempFuzzy[0][counter]) / (f - e);
            } else {
                tabTempFuzzy[5][counter] = 0f;
            }

            // troche wysoka
            if ((tabTempFuzzy[0][counter] >= e) && (tabTempFuzzy[0][counter] <= f)) {
                tabTempFuzzy[6][counter] = (tabTempFuzzy[0][counter] - e) / (f - e);
            } else if ((tabTempFuzzy[0][counter] >= f) && (tabTempFuzzy[0][counter] <= g)) {
                tabTempFuzzy[6][counter] = (g - tabTempFuzzy[0][counter]) / (g - f);
            } else {
                tabTempFuzzy[6][counter] = 0f;
            }

            // wysoka
            if ((tabTempFuzzy[0][counter] >= f) && (tabTempFuzzy[0][counter] <= g)) {
                tabTempFuzzy[7][counter] = (tabTempFuzzy[0][counter] - f) / (g - f);
            } else if ((tabTempFuzzy[0][counter] >= g) && (tabTempFuzzy[0][counter] <= h)) {
                tabTempFuzzy[7][counter] = (h - tabTempFuzzy[0][counter]) / (h - g);
            } else {
                tabTempFuzzy[7][counter] = 0f;
            }

            // bardzo wysoka
            if ((tabTempFuzzy[0][counter] >= g) && (tabTempFuzzy[0][counter] <= h)) {
                tabTempFuzzy[8][counter] = (tabTempFuzzy[0][counter] - g) / (h - g);
            } else if ((tabTempFuzzy[0][counter] >= h) && (tabTempFuzzy[0][counter] <= i)) {
                tabTempFuzzy[8][counter] = (i - tabTempFuzzy[0][counter]) / (i - h);
            } else {
                tabTempFuzzy[8][counter] = 0f;
            }

            // ekstremalnie wysoka
            if ((tabTempFuzzy[0][counter] >= h) && (tabTempFuzzy[0][counter] <= i)) {
                tabTempFuzzy[9][counter] = (tabTempFuzzy[0][counter] - h) / (i - h);
            } else {
                tabTempFuzzy[9][counter] = 0f;
            }
            variable++;

            System.out.println( tabTempFuzzy[0][counter] + " | " +tabTempFuzzy[1][counter] + " | " + tabTempFuzzy[2][counter] + " | " + tabTempFuzzy[3][counter] + " | " + tabTempFuzzy[4][counter] + " | " + tabTempFuzzy[5][counter] + " | " + tabTempFuzzy[6][counter] + " | " + tabTempFuzzy[7][counter] + " | " + tabTempFuzzy[8][counter] + " | " + tabTempFuzzy[9][counter] + " | " );

        }
    }

    /**
     * Metoda obliczająca moc chłodziarki
     */
    public static float CalculatePower(float deltaTemperature) {

        float[] deltaTemperatureIn = new float[9];
        DeltaTempFuzzy();
        DeltaFreezerFuzzy();
        ReadRules();
        writeRules();


        deltaTemperatureIn[0] = tabTempFuzzy[1][(int) ((deltaTemperature + 40) * 10)];
        deltaTemperatureIn[1] = tabTempFuzzy[2][(int) ((deltaTemperature + 40) * 10)];
        deltaTemperatureIn[2] = tabTempFuzzy[3][(int) ((deltaTemperature + 40) * 10)];
        deltaTemperatureIn[3] = tabTempFuzzy[4][(int) ((deltaTemperature + 40) * 10)];
        deltaTemperatureIn[4] = tabTempFuzzy[5][(int) ((deltaTemperature + 40) * 10)];
        deltaTemperatureIn[5] = tabTempFuzzy[6][(int) ((deltaTemperature + 40) * 10)];
        deltaTemperatureIn[6] = tabTempFuzzy[7][(int) ((deltaTemperature + 40) * 10)];
        deltaTemperatureIn[7] = tabTempFuzzy[8][(int) ((deltaTemperature + 40) * 10)];
        deltaTemperatureIn[8] = tabTempFuzzy[9][(int) ((deltaTemperature + 40) * 10)];

        float[] freezerTable = new float[9];

    for (int i =0; i <9; i++) {
        freezerTable[i] = (deltaTemperatureIn[i]);
    }

        float bardzoMocneGrzanie = 0;
        float mocneGrzanie = 0;
        float srednieGrzanie = 0;
        float slabeGrzanie = 0;
        float brakIngerencji = 0;
        float slabeChlodzenie = 0;
        float srednieChlodzenie = 0;
        float mocneChlodzenie = 0;
        float bardzoMocneChlodzenie = 0;


        //Rozmywanie
        for(int i = 0; i < 9; i++){

            if(freezerTable[i] != 0){

                switch(rulesFreezer[i]) {
                    case "bardzo mocne grzanie": {
                        if(freezerTable[i] > bardzoMocneGrzanie)
                            bardzoMocneGrzanie = freezerTable[i];
                        break;
                    }
                    case "mocne grzanie": {
                        if(freezerTable[i] > mocneGrzanie)
                            mocneGrzanie = freezerTable[i];
                        break;
                    }
                    case "srednie grzanie": {
                        if(freezerTable[i] > srednieGrzanie)
                            srednieGrzanie = freezerTable[i];
                        break;
                    }
                    case "slabe grzanie": {
                        if(freezerTable[i] > slabeGrzanie)
                            slabeGrzanie = freezerTable[i];
                        break;
                    }
                    case "brak ingerencji": {
                        if(freezerTable[i] > brakIngerencji)
                            brakIngerencji = freezerTable[i];
                        break;
                    }
                    case "slabe chlodzenie": {
                        if(freezerTable[i] > slabeChlodzenie)
                            slabeChlodzenie = freezerTable[i];
                        break;
                    }
                    case "srednie chlodzenie": {
                        if(freezerTable[i] > srednieChlodzenie)
                            srednieChlodzenie = freezerTable[i];
                        break;
                    }
                    case "mocne chlodzenie": {
                        if(freezerTable[i] > mocneChlodzenie)
                            mocneChlodzenie = freezerTable[i];
                        break;
                    }
                    case "bardzo mocne chlodzenie": {
                        if(freezerTable[i] > bardzoMocneChlodzenie)
                            bardzoMocneChlodzenie = freezerTable[i];
                        break;
                    }

                }
            }

        }

        //Wnioskowanie

        float freezerNominator = 0f;
        float freezerDenominator = 0f;

        float[][] outputFreezer = new float [10][800];

        for(int i = 0; i < outputFreezer[0].length; i++) {
            outputFreezer[0][i] = i / 10f;

            if(bardzoMocneGrzanie > 0) {
                outputFreezer[1][i] = Math.min(tabFreezerFuzzy[1][i], bardzoMocneGrzanie);
            } else
                outputFreezer[1][i] = 0;

            if(mocneGrzanie > 0) {
                outputFreezer[2][i] = Math.min(tabFreezerFuzzy[2][i], mocneGrzanie);
            } else
                outputFreezer[2][i] = 0;

            if(srednieGrzanie > 0) {
                outputFreezer[3][i] = Math.min(tabFreezerFuzzy[3][i], srednieGrzanie);
            } else
                outputFreezer[3][i] = 0;

            if(slabeGrzanie > 0) {
                outputFreezer[4][i] = Math.min(tabFreezerFuzzy[4][i], slabeGrzanie);
            } else
                outputFreezer[4][i] = 0;

            if(brakIngerencji > 0) {
                outputFreezer[5][i] = Math.min(tabFreezerFuzzy[5][i], brakIngerencji);
            } else
                outputFreezer[5][i] = 0;

            if(slabeChlodzenie > 0) {
                outputFreezer[6][i] = Math.min(tabFreezerFuzzy[6][i], slabeChlodzenie);
            } else
                outputFreezer[6][i] = 0;

            if(srednieChlodzenie > 0) {
                outputFreezer[7][i] = Math.min(tabFreezerFuzzy[7][i], srednieChlodzenie);
            } else
                outputFreezer[7][i] = 0;

            if(mocneChlodzenie > 0) {
                outputFreezer[8][i] = Math.min(tabFreezerFuzzy[8][i], mocneChlodzenie);
            } else
                outputFreezer[8][i] = 0;

            if(bardzoMocneChlodzenie > 0) {
                outputFreezer[9][i] = Math.min(tabFreezerFuzzy[9][i], bardzoMocneChlodzenie);
            } else
                outputFreezer[9][i] = 0;



            //Agregacja i wyostrzanie

            freezerNominator += (Math.max(outputFreezer[1][i],
                    Math.max(outputFreezer[2][i],
                            Math.max(outputFreezer[3][i],
                                    Math.max(outputFreezer[4][i],
                                            Math.max(outputFreezer[5][i],
                                                    Math.max(outputFreezer[6][i],
                                                            Math.max(outputFreezer[7][i],
                                                                    Math.max(outputFreezer[8][i],
                                                                            outputFreezer[9][i])))))))))
                    * outputFreezer[0][i];

            freezerDenominator += (Math.max(outputFreezer[1][i],
                    Math.max(outputFreezer[2][i],
                            Math.max(outputFreezer[3][i],
                                    Math.max(outputFreezer[4][i],
                                            Math.max(outputFreezer[5][i],
                                                    Math.max(outputFreezer[6][i],
                                                            Math.max(outputFreezer[7][i],
                                                                    Math.max(outputFreezer[8][i], outputFreezer[9][i])))))))));


        }


        float result = 0;
        result = freezerNominator/freezerDenominator;

        return (result - 50) /10;
    }



    /**
     * Metoda służąca do zczytywania reguł z pliku xls
     */
    public static void ReadRules() {
        rules = new String[9][2];
        rulesFreezer = new String[9];

        try {
            File file = new File("regulyGotowe.xls");

            Workbook workbook = Workbook.getWorkbook(file);

            Sheet sheet = workbook.getSheet(0);
            sheet.getSettings().setProtected(false);

            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 2; j++) {

                    Cell cel = sheet.getCell(j, i + 1);

                    if (cel.getType() == CellType.LABEL) {
                        LabelCell lcel = (LabelCell) cel;
                        rules[i][j] = lcel.getString();
                    }
                }
            }
            workbook.close();
        } catch (Exception e) {

        }
    }

    /**
     * Metoda rozpisująca pobrane metody z pliku xls
     */
    public static void writeRules() {
        int x = 0;
        int y = 0;

        for (int i = 0; i < 9; i++) {

            switch (rules[i][0]) {
                case "ekstremalnie niska": {
                    x = 0;
                    break;
                }
                case "bardzo niska": {
                    x = 1;
                    break;
                }
                case "niska": {
                    x = 2;
                    break;
                }
                case "troche niska": {
                    x = 3;
                    break;
                }
                case "optymalna": {
                    x = 4;
                    break;
                }
                case "troche wysoka": {
                    x = 5;
                    break;
                }
                case "wysoka": {
                    x = 6;
                    break;
                }
                case "bardzo wysoka": {
                    x = 7;
                    break;
                }
                case "ekstremalnie wysoka": {
                    x = 8;
                    break;
                }
                default: {

                    break;
                }
            }
            rulesFreezer[x] = rules[i][1];
        }
    }
}