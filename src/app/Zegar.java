package app;

import java.sql.Time;

public class Zegar {

    /**
     * Czas zegara
     */
    Time zegar;

    /**
     * Konstruktor klasy Zegar
     * inicjalizuje początkową wartość zegara symultora
     */
    @SuppressWarnings("deprecation")
    public Zegar() {

        zegar = new Time(6, 0, 0);

    }

    /**
     * Metoda służąca do zmiany czasu zegara o jedną sekundę czasu symulatora
     */
    public void ZmienZegar() {
        zegar.setTime(zegar.getTime() + 1000);
    }

    /**
     * Metoda zwracjąca aktualny czas symulatora w postaci łańcucha znaków
     *
     * @return Aktualny czas symulatora w postaci łańcucha znaków
     */

    public void DodajGodznie() {
        zegar.setTime(zegar.getTime() + (1000*3600));
    }

    public int zwrocGodzina() {

        return zegar.getHours();
    }



    /**
     * Metoda zwracająca aktualny czas symulatora w postaci 00:00:00
     *
     * @return Aktualny czas symulatora w postaci 00:00:00
     */

    public String zwrocCzas() {

        return zegar.toString();
    }

}
